﻿using System.ComponentModel.DataAnnotations;

namespace eTickets.Models
{
    public class Actor
    {
        [Key]
        public int ActorId { get; set; }
        public string ProfilePhoto { get; set; }
        public string FullName { get; set; }
        public string Bio { get; set; }
        // relationships
        public List<Actor_Movie> Actor_Movies { get; set; }
    }
}
