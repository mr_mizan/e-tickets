﻿namespace eTickets.Models
{
    public enum MoviCategory
    {
        Action=1,
        Comedy,
        Drama,
        Thriller,
        Documentary,
        Horror,
        Cartoon
    }
}
